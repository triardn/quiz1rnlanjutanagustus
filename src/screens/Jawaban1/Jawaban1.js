import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';

const Jawaban1 = () => {
  const [name, setName] = useState('Jhon Doe');

  useEffect(() => {
    setTimeout(() => {
      setName('Asep');
    }, 3000);
  });

  return (
    <View>
      <Text>{name}</Text>
    </View>
  );
};

export default Jawaban1;
