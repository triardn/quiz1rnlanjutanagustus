import React, {useContext} from 'react';
import {StyleSheet, View, Text, FlatList} from 'react-native';
import {RootContext} from '../../index';

const Jawaban2 = () => {
  const state = useContext(RootContext);

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.itemContainer}>
        <View>
          <Text>{item.name}</Text>
        </View>
        <View>
          <Text>{item.position}</Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={state.trainer}
        renderItem={renderItem}
        keyExtractor={(item) => item.name}
      />
    </View>
  );
};

export default Jawaban2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
