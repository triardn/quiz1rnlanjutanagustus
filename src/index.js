import React, {useState, createContext} from 'react';
import {Jawaban2} from './screens/Jawaban2/Jawaban2';

export const RootContext = createContext();

const ContextAPI = () => {
  const [trainer, setTrainer] = useState([
    {
      name: 'Zakky Muhammad Fajar',
      position: 'Trainer 1 React Native Lanjutan',
    },
    {
      name: 'Mukhlis Hanafi',
      position: 'Trainer 2 React Native Lanjutan',
    },
  ]);

  return (
    <RootContext.Provider value={{trainer, setTrainer}}>
      <Jawaban2 />
    </RootContext.Provider>
  );
};

export default ContextAPI;
